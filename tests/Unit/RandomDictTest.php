<?php

namespace Glu\LaravelExtensions\Tests\Unit;

use Glu\LaravelExtensions\Str\RandomDictToTest;
use Illuminate\Support\Str;
use Tests\TestCase;

class RandomDictTest extends TestCase
{
    public function testChooseSymbolFromClass()
    {
        $generator = new RandomDictToTest();
        $classes = [
            'A-Z',
            'a-z',
            '\w',
            '0-1',
            '\d',
            ':%\'"!@#',
            ' ',
            // '\x{0400}-\x{04FF}',
        ];
        foreach ($classes as $class) {
            $generated = '';
            for ($i = 1; $i <= 100; ++$i) {
                $generated .= $generator->chooseSymbolFromClass($class);
            }
            $this->assertEquals(1, \preg_match('/^['.$class.']+$/', $generated), "class: $class, generated: $generated");
        }
    }

    /* public function testGeneratingString()
    {
        for ($i = 1; $i <= 100; ++$i) {
            echo Str::randomDict(20, 'a-z \d')."\n";
        }
    } */
}
