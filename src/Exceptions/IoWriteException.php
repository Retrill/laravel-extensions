<?php

declare(strict_types=1);

namespace Glu\LaravelExtensions\Exceptions;

class IoWriteException extends \Exception
{
}
