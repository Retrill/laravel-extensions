<?php

declare(strict_types=1);

namespace Glu\LaravelExtensions;

use Glu\LaravelExtensions\Exceptions\IoWriteException;

class Io
{
    public static function gracefulRewrite(string $filepath, string $newContent, string $tmpFilePrefix = 'tmp'): bool
    {
        $directory = \dirname($filepath);
        if (
            (!$tmpFile = \tempnam($directory, $tmpFilePrefix)) ||
            !\file_put_contents($tmpFile, $newContent)
        ) {
            throw new IoWriteException('Can\'t write temporary file "'.$tmpFile.'"!');
        }

        if (!\rename($tmpFile, $filepath)) {
            throw new IoWriteException('Can\'t move temporary file to "'.$filepath.'"!');
        }

        return true;
    }

    public static function mkdir(string $pathname, int $mode = 0755, ?string $owner = null): bool
    {
        if (!\is_dir($pathname) && !\mkdir($pathname, $mode, true)) {
            throw new IoWriteException('Can\'t create directory "'.$pathname.'"! Check file permissions.');
        }

        if (!\is_null($owner) && !\chown($pathname, $owner)) {
            return false;
        }

        return \chmod($pathname, $mode);
    }
}
