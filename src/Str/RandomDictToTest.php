<?php

declare(strict_types=1);

namespace Glu\LaravelExtensions\Str;

class RandomDictToTest extends RandomDict
{
    public function chooseSymbolFromClass(string $class)
    {
        return parent::{__FUNCTION__}($class);
    }
}
