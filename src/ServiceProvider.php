<?php

declare(strict_types=1);

namespace Glu\LaravelExtensions;

use Glu\LaravelExtensions\Str\RandomDict;
use Illuminate\Support\Str;

/**
 * This is an Laravel Extensions Service Provider implementation.
 *
 * @author Nuno Maduro <enunomaduro@gmail.com>
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register()
    {
        /*
         * Replace substrings of a given pattern by corresponding array keys.
         *
         * @param  array  $replacements
         * @param  string  $subject
         * @param  string  $pattern
         * @return string
         */
        Str::macro(
            'substitute',
            function (array $replacements, string $subject, $pattern = '/{(?<key>.*?)}/'): string {
                return preg_replace_callback($pattern, function ($match) use (&$replacements) {
                    return $replacements[$match['key']] ?? '';
                }, $subject);
            }
        );

        Str::macro(
            'randomDict',
            function (int $length = 20, string $dict = 'A-Z a-z \d', string $boundarySafeDict = ''): string {
                static $randomDict = null;
                $randomDict ??= new RandomDict();

                return $randomDict->generate($length, $dict, $boundarySafeDict);
            }
        );

        foreach (\glob(__DIR__.'/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}
