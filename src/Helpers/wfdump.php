<?php

namespace wf;

function dump($var, $rewrite = true, $traceIndex = 0)
{
    $label = '';
    if (\is_string($rewrite)) {
        $label = '[LABEL] '.$rewrite.":\n\n";
        $rewrite = false;
    }
    $dumpFile = \base_path().'/dump.php';
    $fileContent = '';

    \ob_start();
    var_dump($var);
    $var = \ob_get_contents();
    \ob_end_clean();

    $content = "\n\n".\date('Y-m-d H:i:s').": ****************** NEXT ITEM ************************\n\n";
    $backTrace = \debug_backtrace();
    $content .= '{from '.\addslashes($backTrace[$traceIndex]['file']).':'.$backTrace[$traceIndex]['line'].'}'."\n\n";
    $content .= $label.$var;
    if ($rewrite) {
        \file_put_contents($dumpFile, $content);
    } elseif ($res = fopen($dumpFile, 'a')) {
        \fwrite($res, $content);
    }
}
