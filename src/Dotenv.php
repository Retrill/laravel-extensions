<?php

declare(strict_types=1);

namespace Glu\LaravelExtensions;

class Dotenv
{
    protected static $content;

    protected static function init()
    {
        if (!isset(static::$content)) {
            static::$content = \file_get_contents(\app()->environmentFilePath());
        }
    }

    public static function set(string $name, string $value)
    {
        static::init();

        $replacementsCnt = 0;
        static::$content = \preg_replace("/^\s*$name\s*=.*$/m", "$name=\"$value\"", static::$content, -1, $replacementsCnt);
        if ($replacementsCnt <= 0) {
            static::$content .= "\n$name=\"$value\"";
        }
    }

    public static function write(string $text): void
    {
        static::init();
        static::$content .= "\n".$text;
    }

    public static function store(): bool
    {
        static::init();

        return Io::gracefulRewrite(\app()->environmentFilePath(), static::$content);
    }
}
